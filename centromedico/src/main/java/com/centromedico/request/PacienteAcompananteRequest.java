package com.centromedico.request;

import lombok.*;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PacienteAcompananteRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    @NonNull
    private Integer idPaciente;
    @NotNull
    private Integer idTipoDocIde;
    @NonNull
    private String nroDocIde;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombres;
    private String fechaNacimiento;
    private Integer idParentesco;
    private String nroTelefonoContacto;
    private String direccion;
    private String codUbigeo;
}
