package com.centromedico.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Data
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PacienteRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    @NonNull
    private Integer idTipoDocIde;
    @NonNull
    private String nroDocIde;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombres;
    private Integer idSexo;
    private String fechaNacimiento;
    private String lugarNacimiento;
    private String direccion;
    private String codUbigeo;
}
