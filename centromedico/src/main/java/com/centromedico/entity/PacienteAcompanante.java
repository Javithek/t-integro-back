package com.centromedico.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tb_paciente_acompanante", schema = "admision")
public class PacienteAcompanante {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_paciente_acompanante")
    private Integer idPacienteAcompanante;
    @Column(name = "id_paciente")
    private Integer idPaciente;
    @Column(name = "id_tipo_docide")
    private Integer idTipoDocIde;
    @Column(name = "no_docide")
    private String nroDocIde;
    @Column(name = "no_apepat")
    private String apellidoPaterno;
    @Column(name = "no_apemat")
    private String apellidoMaterno;
    @Column(name = "no_nombres")
    private String nombres;
    @Column(name = "fe_nacimiento")
    private LocalDate fechaNacimiento;
    @Column(name = "id_parentesco")
    private Integer idParentesco;
    @Column(name = "nu_telefo_contacto")
    private String nroTelefonoContacto;
    @Column(name = "no_direccion")
    private String direccion;
    @Column(name = "co_ubigeo")
    private String codUbigeo;
}
