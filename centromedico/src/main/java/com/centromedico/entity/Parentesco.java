package com.centromedico.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tc_parentesco", schema = "admision")
public class Parentesco {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id_parentesco")
    private Integer idParentesco;
    @Column(name = "no_parentesco")
    private String descripcionParentesco;
    @Column(name = "il_activo")
    private Integer flagActivo;

}
