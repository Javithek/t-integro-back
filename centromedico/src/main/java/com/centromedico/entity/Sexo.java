package com.centromedico.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tc_sexo", schema = "admision")
public class Sexo {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id_sexo")
    private Integer idSexo;
    @Column(name = "descripcion_sexo")
    private String descripcionSexo;
    @Column(name = "fl_estado")
    private Integer flagEstado;
}
