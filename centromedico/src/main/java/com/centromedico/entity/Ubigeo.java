package com.centromedico.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tc_ubigeo", schema = "admision")
public class Ubigeo {
    @Id
    @Column(name = "codigo_ubigeo")
    private String codigoUbigeo;
    @Column(name = "descripcion_departamento")
    private String descripcionDepartamento;
    @Column(name = "descripcion_provincia")
    private String descripcionProvincia;
    @Column(name = "descripcion_distrito")
    private String descripcionDistrito;
    @Column(name = "fl_estado")
    private Integer flagEstado;
    @Column(name = "codigo_departamento")
    private String codigoDepartamento;
    @Column(name = "codigo_provincia")
    private String codigoProvincia;
    @Column(name = "codigo_distrito")
    private String codigoDistrito;
}
