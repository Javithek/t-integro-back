package com.centromedico.entity;

import lombok.*;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tc_tipo_documento_identidad", schema = "admision")
public class TipoDocumento {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id_tipo_documento_identidad")
    private Integer idTipoDocumentoIdentidad;
    @Column(name = "descripcion_tipo_documento_identidad")
    private String descripcionTipoDocumentoIdentidad;
    @Column(name = "codigo_ieds")
    private String codigoIEDS;
    @Column(name = "fl_estado")
    private String flagEstado;
}
