package com.centromedico.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tb_paciente", schema = "admision")
public class Paciente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_paciente")
    private Integer idPaciente;
    @Column(name = "id_tipo_docide")
    private Integer idTipoDocIde;
    @Column(name = "no_docide")
    private String nroDocIde;
    @Column(name = "no_apepat")
    private String apellidoPaterno;
    @Column(name = "no_apemat")
    private String apellidoMaterno;
    @Column(name = "no_nombres")
    private String nombres;
    @Column(name = "id_sexo")
    private Integer idSexo;
    @Column(name = "fe_nacimiento")
    private LocalDate fechaNacimiento;
    @Column(name = "no_lugar_nacimiento")
    private String lugarNacimiento;
    @Column(name = "no_direccion")
    private String direccion;
    @Column(name = "co_ubigeo")
    private String codUbigeo;
}
