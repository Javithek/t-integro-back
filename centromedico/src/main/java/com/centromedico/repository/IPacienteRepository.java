package com.centromedico.repository;

import com.centromedico.entity.Paciente;
import com.centromedico.entity.Parentesco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPacienteRepository extends JpaRepository<Paciente, Integer> {
}
