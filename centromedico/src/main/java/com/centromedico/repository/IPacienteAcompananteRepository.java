package com.centromedico.repository;

import com.centromedico.entity.PacienteAcompanante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPacienteAcompananteRepository extends JpaRepository<PacienteAcompanante, Integer> {
}
