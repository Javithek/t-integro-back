package com.centromedico.repository;

import com.centromedico.entity.Sexo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISexoRepository extends JpaRepository<Sexo, Integer> {
}
