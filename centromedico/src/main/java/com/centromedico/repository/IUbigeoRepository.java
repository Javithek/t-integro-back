package com.centromedico.repository;

import com.centromedico.entity.Ubigeo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUbigeoRepository extends JpaRepository<Ubigeo, Integer> {
}
