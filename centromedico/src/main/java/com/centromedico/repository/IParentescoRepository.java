package com.centromedico.repository;

import com.centromedico.entity.Parentesco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IParentescoRepository extends JpaRepository<Parentesco, Integer> {
}
