package com.centromedico.util;

import com.centromedico.entity.*;
import com.centromedico.model.*;
import com.centromedico.request.PacienteAcompananteRequest;
import com.centromedico.request.PacienteRequest;

import java.time.LocalDate;

public class UtilsWrapper {
    public static TipoDocumentoDTO wrapperTipoDocumento(TipoDocumento tipoDocumento){
        TipoDocumentoDTO tipoDocumentoDTO = new TipoDocumentoDTO();
        tipoDocumentoDTO.setIdTipoDocumentoIdentidad(String.valueOf(tipoDocumento.getIdTipoDocumentoIdentidad()));
        tipoDocumentoDTO.setDescripcionTipoDocumentoIdentidad(tipoDocumento.getDescripcionTipoDocumentoIdentidad());
        tipoDocumentoDTO.setCodigoIEDS(tipoDocumento.getCodigoIEDS());
        tipoDocumentoDTO.setFlagEstado(tipoDocumento.getFlagEstado());
        return tipoDocumentoDTO;
    }

    public static SexoDTO wrapperSexo(Sexo sexo){
        SexoDTO sexoDTO = new SexoDTO();
        sexoDTO.setIdSexo(String.valueOf(sexo.getIdSexo()));
        sexoDTO.setDescripcionSexo(sexo.getDescripcionSexo());
        sexoDTO.setFlagSexo(String.valueOf(sexo.getFlagEstado()));
        return sexoDTO;
    }

    public static UbigeoDTO wrapperUbigeo(Ubigeo ubigeo){
        UbigeoDTO ubigeoDTO = new UbigeoDTO();
        ubigeoDTO.setCodigoUbigeo(ubigeo.getCodigoUbigeo());
        ubigeoDTO.setCodigoDistrito(ubigeo.getCodigoDistrito());
        ubigeoDTO.setCodigoDepartamento(ubigeo.getCodigoDepartamento());
        ubigeoDTO.setCodigoProvincia(ubigeo.getCodigoProvincia());
        ubigeoDTO.setFlagEstado(String.valueOf(ubigeo.getFlagEstado()));
        ubigeoDTO.setDescripcionDepartamento(ubigeo.getDescripcionDepartamento());
        ubigeoDTO.setDescripcionProvincia(ubigeo.getDescripcionProvincia());
        ubigeoDTO.setDescripcionDistrito(ubigeo.getDescripcionDistrito());
        return ubigeoDTO;
    }

    public static ParentescoDTO wrapperParentesco(Parentesco parentesco){
        ParentescoDTO parentescoDTO = new ParentescoDTO();
        parentescoDTO.setIdParentesco(String.valueOf(parentesco.getIdParentesco()));
        parentescoDTO.setDescripcionParentesco(parentesco.getDescripcionParentesco());
        parentescoDTO.setFlagActivo(String.valueOf(parentesco.getFlagActivo()));
        return parentescoDTO;
    }
    public static PacienteDTO wrapperPaciente(Paciente paciente){
        PacienteDTO pacienteDTO = new PacienteDTO();
        pacienteDTO.setIdPaciente(String.valueOf(paciente.getIdPaciente()));
        pacienteDTO.setIdSexo(String.valueOf(paciente.getIdSexo()));
        pacienteDTO.setIdTipoDocIde(String.valueOf(paciente.getIdTipoDocIde()));
        pacienteDTO.setNombres(paciente.getNombres());
        pacienteDTO.setApellidoMaterno(paciente.getApellidoMaterno());
        pacienteDTO.setApellidoPaterno(paciente.getApellidoPaterno());
        pacienteDTO.setDireccion(paciente.getDireccion());
        pacienteDTO.setCodUbigeo(paciente.getCodUbigeo());
        pacienteDTO.setFechaNacimiento(paciente.getFechaNacimiento().toString());
        pacienteDTO.setNroDocIde(paciente.getNroDocIde());
        pacienteDTO.setLugarNacimiento(paciente.getLugarNacimiento());
        return pacienteDTO;
    }
    public static Paciente wrapperEntityPaciente(PacienteRequest pacienteRequest){
        Paciente paciente = new Paciente();
        paciente.setIdSexo((pacienteRequest.getIdSexo() == null || pacienteRequest.getIdSexo() == 0
                ? 1 : pacienteRequest.getIdSexo()));
        paciente.setIdTipoDocIde(pacienteRequest.getIdTipoDocIde());
        paciente.setNombres((pacienteRequest.getNombres() == null || pacienteRequest.getNombres().equals("")
                ? "Name Default" : pacienteRequest.getNombres()));
        paciente.setApellidoPaterno((pacienteRequest.getApellidoPaterno() == null || pacienteRequest.getApellidoPaterno().equals("")
                ? "Apellido Paterno Default" : pacienteRequest.getApellidoPaterno()));
        paciente.setApellidoMaterno((pacienteRequest.getApellidoMaterno() == null || pacienteRequest.getApellidoMaterno().equals("")
                ? "Apellido Materno Default" : pacienteRequest.getApellidoMaterno()));
        paciente.setCodUbigeo((pacienteRequest.getCodUbigeo() == null || pacienteRequest.getCodUbigeo().equals("")
                ? "150101" : pacienteRequest.getCodUbigeo()));
        paciente.setNroDocIde(pacienteRequest.getNroDocIde());
        paciente.setDireccion((pacienteRequest.getDireccion() == null || pacienteRequest.getDireccion().equals("")
                ? "Ubicacion Desconocida" : pacienteRequest.getDireccion()));
        paciente.setLugarNacimiento((pacienteRequest.getLugarNacimiento() == null || pacienteRequest.getLugarNacimiento().equals("")
                ? "Lugar Nacimiento Desconocido" : pacienteRequest.getLugarNacimiento()));
        paciente.setFechaNacimiento(LocalDate.parse((pacienteRequest.getFechaNacimiento().equals("")
                ? "2018-09-19" : pacienteRequest.getFechaNacimiento())));
        return paciente;
    }
    public static PacienteAcompananteDTO wrapperPacienteAcompanante(PacienteAcompanante pacienteAcompanante){
        PacienteAcompananteDTO pacienteAcompananteDTO = new PacienteAcompananteDTO();
        pacienteAcompananteDTO.setIdPaciente(String.valueOf(pacienteAcompanante.getIdPaciente()));
        pacienteAcompananteDTO.setIdPacienteAcompanante(String.valueOf(pacienteAcompanante.getIdPacienteAcompanante()));
        pacienteAcompananteDTO.setIdParentesco(String.valueOf(pacienteAcompanante.getIdParentesco()));
        pacienteAcompananteDTO.setIdTipoDocIde(String.valueOf(pacienteAcompanante.getIdTipoDocIde()));
        pacienteAcompananteDTO.setNombres(pacienteAcompanante.getNombres());
        pacienteAcompananteDTO.setApellidoMaterno(pacienteAcompanante.getApellidoMaterno());
        pacienteAcompananteDTO.setApellidoPaterno(pacienteAcompanante.getApellidoPaterno());
        pacienteAcompananteDTO.setDireccion(pacienteAcompanante.getDireccion());
        pacienteAcompananteDTO.setNroDocIde(pacienteAcompanante.getNroDocIde());
        pacienteAcompananteDTO.setCodUbigeo(pacienteAcompanante.getCodUbigeo());
        pacienteAcompananteDTO.setFechaNacimiento(pacienteAcompanante.getFechaNacimiento().toString());
        pacienteAcompananteDTO.setNroTelefonoContacto(pacienteAcompanante.getNroTelefonoContacto());
        return pacienteAcompananteDTO;
    }
    public static PacienteAcompanante wrapperEntityPacienteAcompanante(PacienteAcompananteRequest pacienteAcompananteRequest){
        PacienteAcompanante pacienteAcompanante = new PacienteAcompanante();
        pacienteAcompanante.setIdPaciente(pacienteAcompananteRequest.getIdPaciente());
        pacienteAcompanante.setIdParentesco((pacienteAcompananteRequest.getIdParentesco() == null || pacienteAcompananteRequest.getIdParentesco() == 0
        ? 1 : pacienteAcompananteRequest.getIdParentesco()));
        pacienteAcompanante.setIdTipoDocIde(pacienteAcompananteRequest.getIdTipoDocIde());
        pacienteAcompanante.setNombres((pacienteAcompananteRequest.getNombres() == null || pacienteAcompananteRequest.getNombres().equals("")
        ? "Nombre Default" : pacienteAcompananteRequest.getNombres()));
        pacienteAcompanante.setApellidoMaterno((pacienteAcompananteRequest.getApellidoMaterno() == null || pacienteAcompananteRequest.getApellidoMaterno().equals("")
        ? "Apellido Materno Default" : pacienteAcompananteRequest.getApellidoMaterno()));
        pacienteAcompanante.setApellidoPaterno((pacienteAcompananteRequest.getApellidoPaterno() == null || pacienteAcompananteRequest.getApellidoPaterno().equals("")
        ? "Apellido Paterno Default" : pacienteAcompananteRequest.getApellidoPaterno()));
        pacienteAcompanante.setNroDocIde(pacienteAcompananteRequest.getNroDocIde());
        pacienteAcompanante.setDireccion((pacienteAcompananteRequest.getDireccion() == null || pacienteAcompananteRequest.getDireccion().equals("")
        ? "Direccion Desconocida" : pacienteAcompananteRequest.getDireccion()));
        pacienteAcompanante.setCodUbigeo((pacienteAcompananteRequest.getCodUbigeo() == null || pacienteAcompananteRequest.getCodUbigeo().equals("")
        ? "150101" : pacienteAcompananteRequest.getCodUbigeo()));
        pacienteAcompanante.setFechaNacimiento(LocalDate.parse((pacienteAcompananteRequest.getFechaNacimiento().equals("")
                ? "2018-09-19" : pacienteAcompananteRequest.getFechaNacimiento())));
        pacienteAcompanante.setNroTelefonoContacto((pacienteAcompananteRequest.getNroTelefonoContacto() == null || pacienteAcompananteRequest.getNroTelefonoContacto().equals("")
        ? "111222333" : pacienteAcompananteRequest.getNroTelefonoContacto()));
        return pacienteAcompanante;
    }
}
