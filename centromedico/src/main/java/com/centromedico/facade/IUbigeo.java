package com.centromedico.facade;

import com.centromedico.model.UbigeoDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IUbigeo {

    public ResponseEntity<List<UbigeoDTO>> getUbigeo();

}
