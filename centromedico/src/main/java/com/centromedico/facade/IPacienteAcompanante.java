package com.centromedico.facade;

import com.centromedico.model.PacienteAcompananteDTO;
import com.centromedico.request.PacienteAcompananteRequest;
import com.centromedico.request.PacienteRequest;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IPacienteAcompanante {
    public ResponseEntity<PacienteAcompananteDTO> crear(PacienteAcompananteRequest pacienteAcompananteRequest);
    public ResponseEntity<List<PacienteAcompananteDTO>> getAcompanantes();
    public ResponseEntity<PacienteAcompananteDTO> actualizar(PacienteAcompananteRequest pacienteAcompananteRequest, Integer id);
}
