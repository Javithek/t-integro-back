package com.centromedico.facade.impl;

import com.centromedico.business.TipoDocumentoBs;
import com.centromedico.facade.ITipoDocumento;
import com.centromedico.model.TipoDocumentoDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/identity")
public class TipoDocumentoImpl implements ITipoDocumento {

    private final TipoDocumentoBs tipoDocumentoBs;

    public TipoDocumentoImpl(TipoDocumentoBs tipoDocumentoBs){
        this.tipoDocumentoBs = tipoDocumentoBs;
    }
    @Override
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/tipo-documento")
    @ResponseBody
    public ResponseEntity<List<TipoDocumentoDTO>> getTipoDocumento() {
        return new ResponseEntity<>(tipoDocumentoBs.getTipoDocumento(), HttpStatus.OK);
    }


}
