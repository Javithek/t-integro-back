package com.centromedico.facade.impl;

import com.centromedico.business.PacienteAcompananteBs;
import com.centromedico.facade.IPacienteAcompanante;
import com.centromedico.model.PacienteAcompananteDTO;
import com.centromedico.request.PacienteAcompananteRequest;
import com.centromedico.request.PacienteRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/acompanante")
public class PacienteAcompananteImpl implements IPacienteAcompanante {
    private final PacienteAcompananteBs pacienteAcompananteBs;
    public PacienteAcompananteImpl(PacienteAcompananteBs pacienteAcompananteBs){
        this.pacienteAcompananteBs = pacienteAcompananteBs;
    }
    @Override
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/crear")
    @ResponseBody
    public ResponseEntity<PacienteAcompananteDTO> crear(@RequestBody PacienteAcompananteRequest pacienteAcompananteRequest) {
        return new ResponseEntity<>(pacienteAcompananteBs.crear(pacienteAcompananteRequest), HttpStatus.CREATED);
    }

    @Override
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/list")
    @ResponseBody
    public ResponseEntity<List<PacienteAcompananteDTO>> getAcompanantes() {
        return new ResponseEntity<>(pacienteAcompananteBs.getAcompanantes(), HttpStatus.OK);
    }

    @Override
    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/actualizar")
    @ResponseBody
    public ResponseEntity<PacienteAcompananteDTO> actualizar(@RequestBody PacienteAcompananteRequest pacienteAcompananteRequest, @RequestParam Integer id) {
        return new ResponseEntity<>(pacienteAcompananteBs.actualizar(pacienteAcompananteRequest, id), HttpStatus.ACCEPTED);
    }

}
