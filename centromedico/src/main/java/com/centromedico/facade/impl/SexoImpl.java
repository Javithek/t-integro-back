package com.centromedico.facade.impl;

import com.centromedico.business.SexoBs;
import com.centromedico.business.impl.SexoBsImpl;
import com.centromedico.facade.ISexo;
import com.centromedico.model.SexoDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/person")
public class SexoImpl implements ISexo {
    private final SexoBs sexoBs;
    public SexoImpl(SexoBs sexoBs){
        this.sexoBs = sexoBs;
    }
    @Override
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/sexo")
    @ResponseBody
    public ResponseEntity<List<SexoDTO>> getSexo() {
        return new ResponseEntity<>(sexoBs.getSexo(), HttpStatus.OK);
    }
}
