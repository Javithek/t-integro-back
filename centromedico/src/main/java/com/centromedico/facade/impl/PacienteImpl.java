package com.centromedico.facade.impl;

import com.centromedico.business.PacienteBs;
import com.centromedico.facade.IPaciente;
import com.centromedico.model.PacienteDTO;
import com.centromedico.request.PacienteRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteImpl implements IPaciente {
    private final PacienteBs pacienteBs;
    public PacienteImpl(PacienteBs pacienteBs){
        this.pacienteBs = pacienteBs;
    }
    @Override
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/crear")
    @ResponseBody
    public ResponseEntity<PacienteDTO> createPaciente(@RequestBody PacienteRequest pacienteRequest) {
        return new ResponseEntity<>(pacienteBs.crearPaciente(pacienteRequest), HttpStatus.CREATED);
    }
    @Override
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/list")
    @ResponseBody
    public ResponseEntity<List<PacienteDTO>> getPacientes() {
        return new ResponseEntity<>(pacienteBs.getPacientes(), HttpStatus.OK);
    }
    @Override
    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/actualizar")
    @ResponseBody
    public ResponseEntity<PacienteDTO> actualizarPaciente(@RequestBody PacienteRequest pacienteRequest, @RequestParam Integer id) {
        return new ResponseEntity<>(pacienteBs.actualizarPaciente(pacienteRequest, id), HttpStatus.ACCEPTED);
    }

}
