package com.centromedico.facade.impl;

import com.centromedico.business.UbigeoBs;
import com.centromedico.facade.IUbigeo;
import com.centromedico.model.UbigeoDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/address")
public class UbigeoImpl implements IUbigeo {
    private final UbigeoBs ubigeoBs;
    public UbigeoImpl(UbigeoBs ubigeoBs){
        this.ubigeoBs = ubigeoBs;
    }
    @Override
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/ubigeo")
    @ResponseBody
    public ResponseEntity<List<UbigeoDTO>> getUbigeo() {
        return new ResponseEntity<>(ubigeoBs.getUbigeo(), HttpStatus.OK);
    }
}
