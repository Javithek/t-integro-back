package com.centromedico.facade.impl;

import com.centromedico.business.ParentescoBs;
import com.centromedico.business.impl.ParentescoBsImpl;
import com.centromedico.facade.IParentesco;
import com.centromedico.model.ParentescoDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/relation")
public class ParentescoImpl implements IParentesco {

    private final ParentescoBs parentescoBs;

    public ParentescoImpl(ParentescoBs parentescoBs){
        this.parentescoBs = parentescoBs;
    }

    @Override
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/parentesco")
    @ResponseBody
    public ResponseEntity<List<ParentescoDTO>> getParentesco() {
        return new ResponseEntity<>(parentescoBs.getParentesco(), HttpStatus.OK);
    }
}
