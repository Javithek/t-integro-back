package com.centromedico.facade;

import com.centromedico.model.SexoDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ISexo {

    public ResponseEntity<List<SexoDTO>> getSexo();

}
