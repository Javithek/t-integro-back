package com.centromedico.facade;

import com.centromedico.model.ParentescoDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IParentesco {

    public ResponseEntity<List<ParentescoDTO>> getParentesco();

}
