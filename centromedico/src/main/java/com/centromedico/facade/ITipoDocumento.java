package com.centromedico.facade;

import com.centromedico.model.TipoDocumentoDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ITipoDocumento {

    public ResponseEntity<List<TipoDocumentoDTO>>  getTipoDocumento();

}
