package com.centromedico.facade;

import com.centromedico.model.PacienteDTO;
import com.centromedico.request.PacienteRequest;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IPaciente {
    public ResponseEntity<PacienteDTO> createPaciente(PacienteRequest pacienteRequest);
    public ResponseEntity<List<PacienteDTO>> getPacientes();
    public ResponseEntity<PacienteDTO> actualizarPaciente(PacienteRequest pacienteRequest, Integer id);
}
