package com.centromedico.model;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class ParentescoDTO {
    private String idParentesco;
    private String descripcionParentesco;
    private String flagActivo;

}
