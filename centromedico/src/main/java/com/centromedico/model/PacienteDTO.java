package com.centromedico.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PacienteDTO {
    private String idPaciente;
    private String idTipoDocIde;
    private String nroDocIde;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombres;
    private String idSexo;
    private String fechaNacimiento;
    private String lugarNacimiento;
    private String direccion;
    private String codUbigeo;

}
