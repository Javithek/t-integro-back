package com.centromedico.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UbigeoDTO {
    private String descripcionDepartamento;
    private String descripcionProvincia;
    private String descripcionDistrito;
    private String flagEstado;
    private String codigoUbigeo;
    private String codigoDepartamento;
    private String codigoProvincia;
    private String codigoDistrito;

}
