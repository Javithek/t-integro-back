package com.centromedico.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TipoDocumentoDTO {

    private String idTipoDocumentoIdentidad;
    private String descripcionTipoDocumentoIdentidad;
    private String codigoIEDS;
    private String flagEstado;

}
