package com.centromedico.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SexoDTO {

    private String idSexo;
    private String descripcionSexo;
    private String flagSexo;

}
