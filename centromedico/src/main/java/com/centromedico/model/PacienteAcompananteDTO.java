package com.centromedico.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PacienteAcompananteDTO {
    private String idPacienteAcompanante;
    private String idPaciente;
    private String idTipoDocIde;
    private String nroDocIde;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombres;
    private String fechaNacimiento;
    private String idParentesco;
    private String nroTelefonoContacto;
    private String direccion;
    private String codUbigeo;
}
