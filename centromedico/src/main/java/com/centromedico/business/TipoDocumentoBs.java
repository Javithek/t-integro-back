package com.centromedico.business;

import com.centromedico.entity.TipoDocumento;
import com.centromedico.model.TipoDocumentoDTO;

import java.util.List;

public interface TipoDocumentoBs {

    public List<TipoDocumentoDTO> getTipoDocumento();

}
