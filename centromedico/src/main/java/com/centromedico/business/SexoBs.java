package com.centromedico.business;

import com.centromedico.model.SexoDTO;

import java.util.List;

public interface SexoBs {

    public List<SexoDTO> getSexo();

}
