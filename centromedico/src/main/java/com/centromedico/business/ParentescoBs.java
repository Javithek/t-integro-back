package com.centromedico.business;

import com.centromedico.model.ParentescoDTO;

import java.util.List;

public interface ParentescoBs {
    public List<ParentescoDTO> getParentesco();

}
