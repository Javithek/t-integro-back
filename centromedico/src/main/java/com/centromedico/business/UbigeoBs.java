package com.centromedico.business;

import com.centromedico.model.UbigeoDTO;

import java.util.List;

public interface UbigeoBs {
    public List<UbigeoDTO> getUbigeo();

}
