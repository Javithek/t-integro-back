package com.centromedico.business.impl;

import com.centromedico.business.ParentescoBs;
import com.centromedico.model.ParentescoDTO;
import com.centromedico.repository.IParentescoRepository;
import com.centromedico.util.UtilsWrapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ParentescoBsImpl implements ParentescoBs {
    private final IParentescoRepository parentescoRepository;
    public ParentescoBsImpl(IParentescoRepository parentescoRepository){
        this.parentescoRepository = parentescoRepository;
    }
    @Override
    public List<ParentescoDTO> getParentesco() {
        return parentescoRepository.findAll().stream().map(UtilsWrapper::wrapperParentesco).collect(Collectors.toList());
    }
}
