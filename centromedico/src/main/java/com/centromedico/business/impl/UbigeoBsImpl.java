package com.centromedico.business.impl;

import com.centromedico.business.UbigeoBs;
import com.centromedico.model.UbigeoDTO;
import com.centromedico.repository.IUbigeoRepository;
import com.centromedico.util.UtilsWrapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UbigeoBsImpl implements UbigeoBs {
    private final IUbigeoRepository ubigeoRepository;
    public UbigeoBsImpl(IUbigeoRepository ubigeoRepository){
        this.ubigeoRepository = ubigeoRepository;
    }
    @Override
    public List<UbigeoDTO> getUbigeo() {
        return ubigeoRepository.findAll().stream().map(UtilsWrapper::wrapperUbigeo).collect(Collectors.toList());
    }
}
