package com.centromedico.business.impl;

import com.centromedico.business.TipoDocumentoBs;
import com.centromedico.entity.TipoDocumento;
import com.centromedico.model.TipoDocumentoDTO;
import com.centromedico.repository.ITipoDocumentoRepository;
import com.centromedico.util.UtilsWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TipoDocumentoBsImpl implements TipoDocumentoBs {

    private final ITipoDocumentoRepository tipoDocumentoRepository;

    public TipoDocumentoBsImpl(ITipoDocumentoRepository tipoDocumentoRepository){
        this.tipoDocumentoRepository = tipoDocumentoRepository;
    }

    @Override
    public List<TipoDocumentoDTO> getTipoDocumento() {
        return tipoDocumentoRepository.findAll().stream().map(UtilsWrapper::wrapperTipoDocumento).collect(Collectors.toList());
    }
}
