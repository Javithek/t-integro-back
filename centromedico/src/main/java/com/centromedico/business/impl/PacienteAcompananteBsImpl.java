package com.centromedico.business.impl;

import com.centromedico.business.PacienteAcompananteBs;
import com.centromedico.entity.PacienteAcompanante;
import com.centromedico.model.PacienteAcompananteDTO;
import com.centromedico.repository.IPacienteAcompananteRepository;
import com.centromedico.request.PacienteAcompananteRequest;
import com.centromedico.util.UtilsWrapper;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PacienteAcompananteBsImpl implements PacienteAcompananteBs {
    private final IPacienteAcompananteRepository pacienteAcompananteRepository;
    public PacienteAcompananteBsImpl(IPacienteAcompananteRepository pacienteAcompananteRepository){
        this.pacienteAcompananteRepository = pacienteAcompananteRepository;
    }
    @Override
    public PacienteAcompananteDTO crear(PacienteAcompananteRequest pacienteAcompananteRequest) {
        PacienteAcompanante pacienteAcompanante = UtilsWrapper.wrapperEntityPacienteAcompanante(pacienteAcompananteRequest);
        PacienteAcompanante pacienteAcompananteSave = pacienteAcompananteRepository.save(pacienteAcompanante);
        PacienteAcompananteDTO pacienteAcompananteReturn = UtilsWrapper.wrapperPacienteAcompanante(pacienteAcompananteSave);
        return pacienteAcompananteReturn;
    }

    @Override
    public List<PacienteAcompananteDTO> getAcompanantes() {
        return pacienteAcompananteRepository.findAll().stream().map(UtilsWrapper::wrapperPacienteAcompanante).collect(Collectors.toList());
    }

    @Override
    public PacienteAcompananteDTO actualizar(PacienteAcompananteRequest pacienteAcompananteRequest, Integer id) {
        return pacienteAcompananteRepository.findById(id)
                .map(pacienteAcompanante -> {
                    pacienteAcompanante.setNombres(pacienteAcompananteRequest.getNombres());
                    pacienteAcompanante.setApellidoPaterno(pacienteAcompananteRequest.getApellidoPaterno());
                    pacienteAcompanante.setApellidoMaterno(pacienteAcompananteRequest.getApellidoMaterno());
                    pacienteAcompanante.setNroDocIde(pacienteAcompananteRequest.getNroDocIde());
                    pacienteAcompanante.setIdTipoDocIde(pacienteAcompananteRequest.getIdTipoDocIde());
                    pacienteAcompanante.setDireccion(pacienteAcompananteRequest.getDireccion());
                    pacienteAcompanante.setNroTelefonoContacto(pacienteAcompananteRequest.getNroTelefonoContacto());
                    pacienteAcompanante.setIdParentesco(pacienteAcompananteRequest.getIdParentesco());
                    pacienteAcompanante.setFechaNacimiento(LocalDate.parse(pacienteAcompananteRequest.getFechaNacimiento()));
                    pacienteAcompanante.setCodUbigeo(pacienteAcompananteRequest.getCodUbigeo());
                    return UtilsWrapper.wrapperPacienteAcompanante(pacienteAcompananteRepository.save(pacienteAcompanante));
                }).orElseThrow(RuntimeException::new);
    }
}
