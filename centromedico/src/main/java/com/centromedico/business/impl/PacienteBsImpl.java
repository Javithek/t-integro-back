package com.centromedico.business.impl;

import com.centromedico.business.PacienteBs;
import com.centromedico.entity.Paciente;
import com.centromedico.model.PacienteDTO;
import com.centromedico.repository.IPacienteRepository;
import com.centromedico.request.PacienteRequest;
import com.centromedico.util.UtilsWrapper;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PacienteBsImpl implements PacienteBs {
    private final IPacienteRepository pacienteRepository;
    public PacienteBsImpl(IPacienteRepository pacienteRepository){
        this.pacienteRepository = pacienteRepository;
    }
    @Override
    public PacienteDTO crearPaciente(PacienteRequest pacienteRequest) {
        Paciente pacienteData = UtilsWrapper.wrapperEntityPaciente(pacienteRequest);
        Paciente pacienteSave = pacienteRepository.save(pacienteData);
        PacienteDTO pacienteReturn = UtilsWrapper.wrapperPaciente(pacienteSave);
        return pacienteReturn;
    }
    @Override
    public List<PacienteDTO> getPacientes() {
        return pacienteRepository.findAll().stream().map(UtilsWrapper::wrapperPaciente).collect(Collectors.toList());
    }

    @Override
    public PacienteDTO actualizarPaciente(PacienteRequest pacienteRequest, Integer id) {
        return pacienteRepository.findById(id)
                .map(paciente -> {
                    paciente.setIdTipoDocIde(pacienteRequest.getIdTipoDocIde());
                    paciente.setNroDocIde(pacienteRequest.getNroDocIde());
                    paciente.setNombres(pacienteRequest.getNombres());
                    paciente.setApellidoPaterno(pacienteRequest.getApellidoPaterno());
                    paciente.setApellidoMaterno(pacienteRequest.getApellidoMaterno());
                    paciente.setIdSexo(pacienteRequest.getIdSexo());
                    paciente.setFechaNacimiento(LocalDate.parse(pacienteRequest.getFechaNacimiento()));
                    paciente.setCodUbigeo(pacienteRequest.getCodUbigeo());
                    paciente.setDireccion(pacienteRequest.getDireccion());
                    paciente.setLugarNacimiento(pacienteRequest.getLugarNacimiento());
                    return UtilsWrapper.wrapperPaciente(pacienteRepository.save(paciente));
                }).orElseThrow(RuntimeException::new);

    }
}
