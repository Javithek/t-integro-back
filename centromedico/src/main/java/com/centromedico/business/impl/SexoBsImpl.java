package com.centromedico.business.impl;

import com.centromedico.business.SexoBs;
import com.centromedico.model.SexoDTO;
import com.centromedico.repository.ISexoRepository;
import com.centromedico.util.UtilsWrapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SexoBsImpl implements SexoBs {
    private final ISexoRepository sexoRepository;
    public SexoBsImpl(ISexoRepository sexoRepository){
        this.sexoRepository = sexoRepository;
    }
    @Override
    public List<SexoDTO> getSexo() {
        return sexoRepository.findAll().stream().map(UtilsWrapper::wrapperSexo).collect(Collectors.toList());
    }
}
