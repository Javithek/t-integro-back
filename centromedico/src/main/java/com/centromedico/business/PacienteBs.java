package com.centromedico.business;

import com.centromedico.entity.Paciente;
import com.centromedico.model.PacienteDTO;
import com.centromedico.request.PacienteRequest;

import java.util.List;

public interface PacienteBs {
    public PacienteDTO crearPaciente(PacienteRequest pacienteRequest);
    public List<PacienteDTO> getPacientes();
    public PacienteDTO actualizarPaciente(PacienteRequest pacienteRequest, Integer id);
}
