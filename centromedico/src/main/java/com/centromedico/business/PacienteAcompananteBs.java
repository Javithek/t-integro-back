package com.centromedico.business;

import com.centromedico.model.PacienteAcompananteDTO;
import com.centromedico.request.PacienteAcompananteRequest;

import java.util.List;

public interface PacienteAcompananteBs {

    public PacienteAcompananteDTO crear(PacienteAcompananteRequest pacienteAcompananteRequest);
    public List<PacienteAcompananteDTO> getAcompanantes();
    public PacienteAcompananteDTO actualizar(PacienteAcompananteRequest pacienteAcompananteRequest, Integer id);

}
